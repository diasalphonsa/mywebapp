FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /build_folder

COPY . /build_folder

RUN dotnet restore

RUN dotnet publish -o /build_folder/published-app

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine as runtime

WORKDIR /main_folder

COPY --from=build /build_folder/published-app /main_folder

ENTRYPOINT [ "dotnet", "/main_folder/MyAPP.dll" ]
